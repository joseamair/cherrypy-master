import cherrypy


# Generates the object and exposes it.
@cherrypy.expose
class StringGeneratorWebService(object):
    """
    This class stores the class to instaciate the app
    """

    # Aux Vars for the class
    request_num = 0

    @cherrypy.tools.accept(media='text/plain')
    def GET(self, client=None):

        """
        This Functions Overrides the behave of the Default GET response being
        made to the default URL of the server.  /localhost/<param>
        :param client: url client name parameter
        :return:
        """

        # Creates the session dictionary counter for each client
        if 'count' not in cherrypy.session:
            cherrypy.session['count'] = 0
        cherrypy.session['count'] += 1

        # increments the Counter for total request made by clients
        StringGeneratorWebService.request_num += 1
        print('Total requests to master = ',
              StringGeneratorWebService.request_num)

        # Outputs the Cliente and number of the request being made
        print('current client: ', client, 'request num:',
              cherrypy.session['count'])

        # returns a message to the client
        return 'Request Acepted for: ' + str(client)


if __name__ == '__main__':

    conf = {
        '/': {
            'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
            'tools.sessions.on': True,
            'tools.response_headers.on': True,
            'tools.response_headers.headers': [('Content-Type', 'text/plain')],
        }
    }
    cherrypy.quickstart(StringGeneratorWebService(), '/', conf)
