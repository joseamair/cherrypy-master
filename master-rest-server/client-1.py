import random
import requests
import string
import time

# Creates the request session
s = requests.Session()

# Generates a random string to set a name for the client
random_string = ''.join(random.choice(string.ascii_letters) for m in range(8))

# client name
print('Client: ', random_string, '\n')

# Sucessfill request
successfull_requests = 0

# Bug and Error (simple) counter
bug_error_count = 0

# Global Settings
request_limit = 10
sleep_time = 5

# loops n time to make a get request to get a response from the server as a
# client
for i in range(1, request_limit):

    try:
        r = s.get('http://127.0.0.1:8080/' + random_string + '/')

        successfull_requests += 1
        print(
            r.status_code,
            r.text,
            'Success: ', successfull_requests,
            'Fails: ', bug_error_count,
            'Remaining: ',
            request_limit - successfull_requests - bug_error_count - 1
        )
        time.sleep(sleep_time)

    except Exception as err:
        bug_error_count += 1
        print('Error counter: ', bug_error_count)
        print('Something Wrong happend')
        print('Error: ', str(err))
        print('Check the running server or Request Params.')
        print('Restarting connection to Server after Time Sleep (5secs)')
        time.sleep(sleep_time)

        # Future Further error handling
        pass
